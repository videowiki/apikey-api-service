const DB_CONNECTION = process.env.APIKEY_SERVICE_DATABASE_URL;
const mongoose = require('mongoose')
const videowikiGenerators = require('@videowiki/generators');
const { server, app, createRouter } = require('./generateServer')();

const controller = require('./controller');
const middlewares = require('./middlewares');

let mongoConnection;
mongoose.connect(DB_CONNECTION)
.then((con) => {
    mongoConnection = con.connection;

    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })

    videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection })

    app.use('/db', require('./dbRoutes')(createRouter()))

    app.all('*', (req, res, next) => {
        if (req.headers['vw-user-data']) {
            try {
                const user = JSON.parse(req.headers['vw-user-data']);
                req.user = user;
            } catch (e) {
                console.log(e);
            }
        }
        next();
    })


    app.get('/', middlewares.authorizeOrganizationAdmin('query'), controller.get)
    app.post('/', middlewares.authorizeOrganizationAdmin('body'), middlewares.validateOrigins, middlewares.validatePermissions, controller.create)
    app.delete('/:apiKeyId', middlewares.authorizeDeletekey, controller.delete)

    app.get('/by_key', controller.getApiKeyByKey)
    app.get('/userKey', controller.getUserOrganizationKey)

})
.catch(err => {
    console.log('Error initiating mongodb connection', err);
    process.exit(1);
})


const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
