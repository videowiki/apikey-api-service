const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ApiKeySchema = new Schema({
    organization: { type: Schema.Types.ObjectId, index: true },
    user: { type: Schema.Types.ObjectId, index: true },
    keyType: { type: String, enum: ['platform', 'service'], default: 'platform' },

    key: String,
    secret: String,
    origins: [String],
    active: { type: Boolean, default: true },
    userKey: { type: Boolean, default: false },
    
    created_at: { type: Number, default: Date.now },
})

const ApiKey = mongoose.model('apiKey', ApiKeySchema)

module.exports = { ApiKey };