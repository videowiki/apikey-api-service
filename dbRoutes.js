const ApiKey = require('./models').ApiKey;
const utils = require('./utils');
module.exports = router => {

    router.get('/generateApiKey', (req, res) => {
        return res.json(utils.generateApiKey());
    })

    router.get('/count', (req, res) => {
        ApiKey.count(req.query)
            .then(count => res.json(count))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message)
            })

    })

    // CRUD routes
    router.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest } = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }
        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        if (one) {
            q = ApiKey.findOne(rest);
        } else {
            q = ApiKey.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((apiKeys) => {
            return res.json(apiKeys);
        })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.post('/', (req, res) => {
        const data = req.body;
        ApiKey.create(data)
            .then((apiKey) => {
                return res.json(apiKey);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        ApiKey.update(conditions, { $set: values }, { ...options, multi: true })
            .then(() => ApiKey.find(conditions))
            .then(apiKeys => {
                return res.json(apiKeys);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/', (req, res) => {
        let conditions = req.body;
        let apiKeys;
        ApiKey.find(conditions)
            .then((a) => {
                apiKeys = a;
                return ApiKey.remove(conditions)
            })
            .then(() => {
                return res.json(apiKeys);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.get('/:id', (req, res) => {
        ApiKey.findById(req.params.id)
            .then((apiKey) => {
                return res.json(apiKey);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        ApiKey.findByIdAndUpdate(id, { $set: changes })
            .then(() => ApiKey.findById(id))
            .then(apiKey => {
                return res.json(apiKey);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/:id', (req, res) => {
        const { id } = req.params;
        let deletedApiKey;
        ApiKey.findById(id)
            .then(apiKey => {
                deletedApiKey = apiKey;
                return ApiKey.findByIdAndRemove(id)
            })
            .then(() => {
                return res.json(deletedApiKey);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    return router;
}